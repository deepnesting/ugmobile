export const state = () => ({
  list: []
});

export const mutations = {
  setTickets(state, tickets) {
    state.list = tickets;
  },
  shiftTicket(state) {
    console.log("shift array");
    state.list.shift();
  }
};

export const getters = {
  topTicket: state => () => state.list[0]
};

export const actions = {
  hideTicket({ commit, state }, ticketId) {
    const res = [];
    for (let i = 0; i < state.list.length; i++) {
      if (state.list[i].id === ticketId) {
        continue;
      }
      res.push(state.list[i]);
    }

    // for (var i = 0; i < array.length; i++) {
    //   array[i]
    // }
    // const prizes = await this.$axios.$get("prizes")
    commit("setTickets", res);
  },
  async fetchTickets({ commit }) {
    const { result } = await this.$api("tickets.list", {
      search: { status: 1 }
    });
    commit("setTickets", result);
  }
};
