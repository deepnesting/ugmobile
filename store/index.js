import Vue from "vue";

export const state = () => ({
  sidebar: false,
  title: "Уютное гнёздышко",
  errorSnackbar: {
    show: false,
    text: "",
    color: "success"
  },
  socket: {}
});

const showSnackbar = (state, text, color) => {
  state.errorSnackbar.color = color;
  state.errorSnackbar.text = text;
  state.errorSnackbar.show = true;
};

export const mutations = {
  toggleSidebar(state) {
    state.sidebar = !state.sidebar;
  },
  setTitle(state, title) {
    state.title = title;
  },
  showSnackbar(state, text) {
    return showSnackbar(state, text, "success");
  },
  showErrorSnackbar(state, text) {
    return showSnackbar(state, text, "error");
  },
  hideSnackbar(state) {
    state.errorSnackbar.show = false;
    state.errorSnackbar.color = "success";
  },
  SOCKET_ONOPEN(state, event) {
    Vue.prototype.$socket = event.currentTarget;
    state.socket.isConnected = true;
  },
  SOCKET_ONCLOSE(state) {
    state.socket.isConnected = false;
  },
  SOCKET_ONERROR(/* state, event */) {},
  // default handler called for all methods
  SOCKET_ONMESSAGE(state, message) {
    //  console.log(state,message)
    try {
      const msg = JSON.parse(message.data);
      switch (msg.type) {
        case 1: // need ticket moderation
          const finded = state.moderation.list.find(
            x => x.id === msg.payload.id
          );
          console.log(
            "new  moderation ticket",
            msg.payload.id,
            finded,
            state.moderation.list.map(x => x.id)
          );
          if (!finded) {
            state.moderation.list.push(msg.payload);
          }
          break;
        default:
          console.log("incoming ws message", msg);
      }
    } catch (e) {
      //
    }
  },
  // mutations for reconnect methods
  SOCKET_RECONNECT(/* state, count */) {},
  SOCKET_RECONNECT_ERROR(state) {
    state.socket.reconnectError = true;
  }
};

export const actions = {
  sendMessage(context, message) {
    Vue.prototype.$socket.send(message);
  }
};
