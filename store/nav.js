export const state = () => ({
  showTabs: false,
  tabs: null,
  model: null,
  dotMenu: null
});

export const mutations = {
  hideTabs(state) {
    state.tabs = null;
    state.showTabs = false;
    state.model = null;
  },
  showTabs(state, tabs, model) {
    state.model = model;
    state.tabs = tabs;
    state.showTabs = true;
  },
  setModel(state, model) {
    state.model = model;
  },
  hideDotMenu(state) {
    state.dotMenu = null;
  },
  setDotMenu(state, menu) {
    state.dotMenu = menu;
  }
};
