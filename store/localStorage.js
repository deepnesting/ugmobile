export const state = () => ({
  city: { title: "Санкт-Петербург", where: "Санкт-Петербурге", id: 1 },
  mapBounds: { coords: [59.95, 30.3], zoom: 8 },
  selectedFilters: {
    rentDuration: 3,
    buildType: 1,
    searchType: 1,
    priceFrom: null,
    priceTo: null,
    metros: [],
    ids: [],
    offset: 0
  },
  faveList: [],
  moderationSuggestHideList: []
});

export const mutations = {
  setCity: (state, city) => (state.city = city),
  setMapBounds: (state, mapBounds) => (state.mapBounds = mapBounds),
  setSelectedFilters: (state, selectedFilters) => {
    selectedFilters.offset = 0;
    state.selectedFilters = selectedFilters;
  },
  toggleFave(state, offerId) {
    if (!state.faveList.includes(offerId)) {
      state.faveList.push(offerId);
    } else {
      state.faveList = state.faveList.filter(x => x !== offerId);
    }
  },
  moderationHideVkOffer(state, vkID) {
    state.moderationSuggestHideList.push(vkID);
  }
};

export const actions = {};

export const getters = {
  getCity: state => () => state.city,
  isFave: (state, offerId) => () => state.faveList.find(x => x === offerId),
  isModerationVkHidden: (state, offerId) => () =>
    state.moderationSuggestHideList.find(x => x === offerId)
};
