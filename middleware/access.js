export default function({ store, redirect, route }) {
  if (route.path.startsWith("/moderation")) {
    if (!(store.$auth.loggedIn && store.$auth.user.isModerator)) {
      return redirect("/");
    }
  }
}
