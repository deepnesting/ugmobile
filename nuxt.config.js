export default {
  server: {
    host: "0.0.0.0"
  },
  mode: "spa",
  /*
   ** Headers of the page
   */
  head: {
    title: "Уютное гнёздышко - найди своё место.",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { name: "theme-color", content: "#038992" },
      {
        hid: "description",
        name: "description",
        content: "Уютное гнёздышко поможет найти жильё, арендаторов и соседей."
      },

      { name: "yandex-verification", content: "d49314b316e120bb" }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons"
      },
      {
        rel: "stylesheet",
        href: "https://use.fontawesome.com/releases/v5.1.0/css/all.css"
      }
    ],
    script: [{ src: "https://apis.google.com/js/platform.js", async: true }]
  },
  modules: [
    "@nuxtjs/axios",
    "@nuxtjs/auth",
    "@nuxtjs/toast",
    "@nuxtjs/proxy",
    "nuxt-vuex-localstorage",
    ["@nuxtjs/moment", { locales: ["ru"], defaultLocale: "ru" }],
    [
      "@nuxtjs/google-analytics",
      {
        id: "UA-141108774-1"
      }
    ],
    [
      "vue-yandex-maps/nuxt",
      {
        apiKey: "b8f5ffe3-18f0-4368-bae4-3cf3ea55f650",
        lang: "ru_RU",
        version: "2.1"
      }
    ]
  ],
  buildModules: [
    // Simple usage
    "@nuxtjs/vuetify"
  ],
  vuetify: {
    treeShake: true,
    theme: {
      themes: {
        light: {
          primary: "#038992",
          secondary: "#b0bec5",
          accent: "#8c9eff",
          error: "#b71c1c"
        }
      }
    }
  },
  plugins: [
    // '~/plugins/vuetify.js',
    "~/plugins/api.js",
    "~/plugins/google.js",
    { src: "~/plugins/ws.js", ssr: false },
    { src: "~/plugins/avatar-cropper.js", ssr: false },
    "~/plugins/observe-visiblity.js"
  ],
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: "/api/v1/auth/login",
            method: "post",
            propertyName: "token"
          },
          logout: false, // { url: "/api/v1/auth/logout", method: "post" },
          user: {
            url: "/api/v1/users/me",
            method: "get",
            propertyName: "user"
          }
        },
        tokenRequired: true
      }
    }
  },
  router: {
    middleware: "access"
  },
  axios: {
    proxy: true,
    // browserBaseURL: "",
    // baseURL: process.env.BASE_URL || "http://localhost:3000",
    debug: true
  },
  proxy: [
    [
      process.env.REST_PROXY_URL || "http://localhost:8000/api/v1",
      { changeOrigin: true }
    ],
    [
      process.env.RPC_PROXY_URL || "http://localhost:8000/rpc/v1",
      { changeOrigin: true }
    ],
    ["http://localhost:8000/images", { changeOrigin: true }]
  ],
  toast: {
    duration: 3000
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#038992" },
  /*
   ** Build configuration
   */
  build: {
    extractCSS: true,
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        });
      }
    }
  }
};
